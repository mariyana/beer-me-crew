function trackEvent(config) {

    window._paq = window._paq || [];

    _paq.push(config);
}

var animateButton = function(e) {

    e.preventDefault;
    //reset animation
    e.target.classList.remove('animate');

    e.target.classList.add('animate');
    setTimeout(function(){
        e.target.classList.remove('animate');
    },900);
};

function handleButton1Click() {
    var btn = document.getElementById("btn-1");

    if (!btn) return;

    btn.addEventListener('click', function (e) {
        var config = [
            'trackEvent',
            'click',
            'share',
            this.innerHTML
        ];

        trackEvent(config);
        animateButton(e);

    }, false);
}

function handleButton2Click() {
    var btn = document.getElementById("btn-2");

    if (!btn) return;

    btn.addEventListener('click', function (e) {
        var config = [
            'trackEvent',
            'navigate',
            'click external link',
            this.innerHTML
        ];

        trackEvent(config);
        animateButton(e);

    }, false);
}

document.addEventListener("DOMContentLoaded", function() {
    handleButton1Click();
    handleButton2Click();
});